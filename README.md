# Missionaries and Cannibals puzzle solver

The missionaries and cannibals puzzle is one of several classic 
river-crossing problems. The initial state of the problem describes 
3 missionaries on one side of a river along with 3 cannibals.  There is boat
that can hold one or two people at a time. We need to find a way to get
everyone to the opposite side of the river without ever leaving a group of
missionaries in one place outnumbered by the cannibals in that place.

## Installation

Simply download the source code and run <make> from the command line from
within the src/ folder

## Usage

Compiling the source code will produce the executable file <main>.

Run <main> from command line to produce output that visually displays the 
order of steps to solve the puzzle.

## Credits

Written by Cody Barnson


#include <iostream>
#include <vector>
#include <string>
#include <stack>

#include "Node.h"

//global vector to hold 'tree' of moves
std::vector<Node*> gamePath;

void expand(Node*);
void kill_repeated_or_illegal(Node*);
bool is_illegal(int, int);

int main()
{
   bool gameOver = false;
   
   //create our starting point with everyone and the boat on the left
   Node* river = new Node(3, 3, true, nullptr);	
   gamePath.push_back(river);
   std::cout << "Missionaries and Cannibals Puzzle\n" <<
      "---------------------------------\n";
   
   //main game loop - breadth first search
   int i = 0;
   while (!gameOver) {
      expand(gamePath[i++]);
      if (gamePath[i]->getM() == 0 && gamePath[i]->getC() == 0)
	 gameOver = true;
   }
   
   //gameEnd points to the goal state, trace through tree via parents
   Node* gameEnd = gamePath[i];
   std::stack<Node*> finalPath;
   
   //push onto a stack representing our path step-by-step
   int total_steps = 0;
   while (gameEnd->getParent()) { 
      finalPath.push(gameEnd);
      gameEnd = gameEnd->getParent();
      total_steps++;
   }
   finalPath.push(gameEnd);
   
   //display our steps
   while (!finalPath.empty()) {
      finalPath.top()->print();
      finalPath.pop();
   }
   std::cout << "total steps taken to reach goal: " << total_steps << "\n";
   return 0;
}

void expand(Node* n) {
   int m = n->getM(); //missionaries on the left side
   int c = n->getC(); //cannibals on the left side

   //check for base case
   if ((m == 0) && (c == 0))
      return; //n is the pointer to the answer

   /***********************************/
   //actions - expand all possible moves
   /***********************************/
   if (n->getBoat()) { //boat left

      //note child's boat changes sides true -> false
	   
      //move one missionary across
      if (m > 0) {
	 m -= 1;
	 n->addChild(new Node(m, c, false, n));
	 m += 1;
      }
      //move one cannibal across
      if (c > 0) {
	 c -= 1;
	 n->addChild(new Node(m, c, false, n));
	 c += 1;
      }
      //move two missionaries across
      if (m > 1) {
	 m -= 2;
	 n->addChild(new Node(m, c, false, n));
	 m += 2;
      }
      //move two cannibals across
      if (c > 1) {
	 c -= 2;
	 n->addChild(new Node(m, c, false, n));
	 c += 2;
      }
      //move one of each
      if ((m > 1) && (c > 1)) {
	 m -= 1;
	 c -= 1;
	 n->addChild(new Node(m, c, false, n));
	 m += 1;
	 c += 1;
      }
   }
   if (!(n->getBoat())) {
      //boat is on right side
      //note that boat changes sides false -> true
      //move one missionary across
      if (m < 3) {
	 m += 1;
	 n->addChild(new Node(m, c, true, n));
	 m -= 1;
      }
      //move one cannibal across
      if (c < 3) {
	 c += 1;
	 n->addChild(new Node(m, c, true, n));
	 c -= 1;
      }
      //move two missionaries across
      if (m < 2) {
	 m += 2;
	 n->addChild(new Node(m, c, true, n));
	 m -= 2;
      }
      //move two cannibals across
      if (c < 2) {
	 c += 2;
	 n->addChild(new Node(m, c, true, n));
	 c -= 2;
      }
      //move one of each
      if ((m < 3) && (c < 3)) {
	 m += 1;
	 c += 1;
	 n->addChild(new Node(m, c, true, n));
	 m -= 1;
	 c -= 1;
      }
   }
	
   //remove any repeated or illegal states resulting from possible moves
   kill_repeated_or_illegal(n);

   //copy the new nodes (that passed the tests) into gamePath
   std::vector<Node*> legal_options = n->getChildren();
   for (int i = legal_options.size() - 1; i >= 0; i--) {
      gamePath.push_back(legal_options[i]);
   }
}

//takes a pointer to a node with children, i.e. possible moves from a state
//eliminates those that are repeats of previous states, prevent infinite loops
void kill_repeated_or_illegal(Node* n) {
   std::vector<Node*> all_options = n->getChildren();
   
   //kill illegal - from back to front otherwise the order is compromised
   for (int i = all_options.size() - 1; i >= 0; i--) {
      if (is_illegal(all_options[i]->getM(), all_options[i]->getC()))
	 n->killChild(i);
   }

   //check resulting legal options against those in our global gamePath
   //kill any repeated states, i.e. those already in gamePath
   for (int i = gamePath.size() - 1; i >= 0; i--) {
      all_options = n->getChildren(); 
      for (int j = all_options.size() - 1; j >= 0; j--) {	 
	 if ((all_options[j]->getM() == gamePath[i]->getM()) &&
	     (all_options[j]->getC() == gamePath[i]->getC()) &&
	     (all_options[j]->getBoat() == gamePath[i]->getBoat()))
	    n->killChild(j);
      }
   }
}

//returns true if the number of cannibals outnumber the number of missionaries
//for either side of the river
bool is_illegal(int m, int c) {
   if (((m > 0) && (c > m)) || ((m < 3) && ((3 - c) >(3 - m))))
      return true;
   return false;
}

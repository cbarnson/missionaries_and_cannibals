#ifndef NODE_H
#define NODE_H

#include <vector>

class Node {
  public:
   Node();
   Node(int m, int c, bool boat, Node*);
   ~Node();

   //set methods
   void setState(int m, int c);
   void setBoat(bool);
   void setParent(Node*);     

   //get methods
   int getM();
   int getC();
   bool getBoat();
   Node* getParent();
   std::vector<Node*> getChildren();

   //other functions
   void addChild(Node*);
   void killChild(int);
   void print();

  private:
   int m_left;     //# of missionaries on the left shore
   int c_left;     //# of cannibals on the left shore
   bool boat_left; //true if boat is located on the left shore
   Node* parent;
   std::vector<Node*> children; //represents next-move options
};

#endif


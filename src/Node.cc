#include <iostream>
#include <vector>
#include <string>
#include "Node.h"


Node::Node() {

}

Node::Node(int m, int c, bool b, Node* p) {
   m_left = m;
   c_left = c;
   boat_left = b;
   parent = p;
}

Node::~Node() {

}

//*****************************************************************
//set methods
//*****************************************************************
void Node::setState(int m, int c) {
   m_left = m;
   c_left = c;
}

void Node::setBoat(bool b) {
   boat_left = b;
}

void Node::setParent(Node* n) {
   parent = n;
}

//push onto children vector
//represents 'possible' options for the next-move from that state
void Node::addChild(Node* n) {
   children.push_back(n);
}

//remove child from indexed position
//filters out illegal moves or repeated states
void Node::killChild(int index) {
   children.erase(children.begin() + index);
}

//*****************************************************************
//get methods
//*****************************************************************
int Node::getM() {
   return m_left;
}

int Node::getC() {
   return c_left;
}

bool Node::getBoat() {
   return boat_left;
}

Node* Node::getParent() {
   return parent;
}

std::vector<Node*> Node::getChildren() {
   return children;
}

//print the current state
//displays the number of missionaries and and cannibals for each side,
//as well as the side of the river the boat is on
void Node::print() {
   std::cout << std::string(m_left, 'M') << "\t~ ~ ~\t";
   std::cout << std::string(3 - m_left, 'M') << "\n";
   std::cout << std::string(c_left, 'C') << "\t ~ ~ \t";
   std::cout << std::string(3 - c_left, 'C') << "\n";
   if(boat_left)
      std::cout << "|__|    ~ ~ ~ \n";
   else
      std::cout << "\t~ ~ ~ \t|__|\n";
   std::cout << "\n";
}





